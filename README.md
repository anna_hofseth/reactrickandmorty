# ReactRickAndMorty

##About
This is a website with a collection of Rick And Morty characters.

##Setup
- Clone repo
- Have node installed
- cd to repo
- Run command:
```
npm install
```
- Run command:
```
npm start
```

##Build for production

```
npm run build
```