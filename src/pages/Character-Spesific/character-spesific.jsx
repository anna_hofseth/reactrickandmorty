import React from "react";
import CharacterSpesificComponent from "../../components/Character-Spesific-Component/Character-Spesific-Component";

export default class CharacterSpesific extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			rmObject: [],
			characterId: this.props.match.params.id
		}
	}

	componentDidMount(){
		const app = this;
		app.getData();
	}

	getData(){
		const app = this;
		fetch("https://rickandmortyapi.com/api/character/"+app.state.characterId)
		.then(response => {
			return response.json()
		})
		.then(result => {
			app.setState({
				rmObject: result
			})

			console.log(result);
		});
	}

	render() {
		const app = this;
		let spesificCharacter = <CharacterSpesificComponent image={app.state.rmObject.image}
															name={app.state.rmObject.name}
															species={app.state.rmObject.species}>
								</CharacterSpesificComponent>

		return (
			<div className="row">
				<div className="col-sm-12">
					<h2>CharcterSpesific</h2>
				</div>
			</div>
		);
	}
}