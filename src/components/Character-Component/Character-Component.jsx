import React from "react";

export default class CharacterComponent extends React.Component {
	render(){
		return (
			<div className="col-sm-4">
				<div className="card">
					<a href={"#/character-spesific/"+this.props.id}>
						<h3>{this.props.name}</h3>
						<div><img className="card__image" src={this.props.image}/></div>
						<div className="card__specs">
							<p><b className="card__specs__label">Species: </b>{this.props.species}</p>
							<p><b className="card__specs__label">Status: </b>{this.props.status}</p>
							<p><b className="card__specs__label">Gender: </b>{this.props.gender}</p>
							<p><b className="card__specs__label">Location: </b>{this.props.location}</p>
							<p><b className="card__specs__label">Place of Origin: </b>{this.props.origin}</p>
						</div>
					</a>
				</div>
			</div>
		);
	}
}